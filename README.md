Olá!

Sou Mariana Gomes este repositório visa manter amazenado estudos de novas
tecnologias de acordo com as tendências do mercado. Tenha em mente que nem todos os códigos 
podem funcionar nem são padronizados de acordo com os padrões atuais de software que existem hoje. 
Sinta-se à vontade para revisar os códigos e aprender com eles.

Renda-se, como eu me rendi. Mergulhe no que você não conhece como eu mergulhei. Não se preocupe em entender, viver ultrapassa qualquer entendimento.

Clarice Lispector
Nota: Trecho citado por Anna Maria Knobel em "Moreno Em Ato", atribuído a Clarice Lispector.


GPL-3.0 © Mariana Gomes